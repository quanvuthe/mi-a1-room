const util = require('util');
const exec = util.promisify(require('child_process').exec);

const run = async (cmd) => {
  try {
    console.log('run =>', cmd);
    const { stdout, stderr } = await exec(cmd, {
      maxBuffer: 1024 * 1024 * 1024,
    });
    return { stdout, stderr };
  } catch (err) {
    return { stdout: '', stderr: err.message };
  }
};
const getDevices = async () => {
  try {
    const adbDevices =
      (await run('adb devices'))?.stdout
        ?.match(/(\w+)\t/g)
        ?.map((x) => ({ serial: x.replace('\t', ''), type: 'adb' }))
        ?.sort() || [];
    const fastbootDevices =
      (await run('fastboot devices'))?.stdout
        ?.match(/(\w+)\t/g)
        ?.map((x) => ({ serial: x.replace('\t', ''), type: 'fastboot' }))
        ?.sort() || [];
    return [...adbDevices, ...fastbootDevices];
  } catch (err) {
    console.log({ err });
    return [];
  }
};
const sleep = (timeout) =>
  new Promise((res) => {
    setTimeout(() => {
      res();
      console.log('sleep', timeout);
    }, timeout);
  });

const setup = async (devices) => {
  const runingDevices = devices.map(
    (device) =>
      new Promise(async (res) => {
        const { serial } = device;
        await run(`adb -s ${serial} reboot recovery`);
        await run(`adb -s ${serial} wait-for-recovery`);
        await run(`adb -s ${serial} shell "timeout -s 7 twrp wipe data"`);
        await run(`adb -s ${serial} shell "timeout -s 7 twrp wipe cache"`);
        await run(`adb -s ${serial} shell "timeout -s 7 twrp wipe dalvik"`);
        await run(`adb -s ${serial} shell "timeout -s 7 twrp wipe /system"`);
        await run(`adb -s ${serial} shell sleep 5`);
        await run(`adb -s ${serial} push S7/Rom /sdcard/`);
        await run(`adb -s ${serial} shell sleep 5`);
        await run(`adb -s ${serial} shell twrp install /sdcard/Rom/super_rom_S7.zip`);
        await run(`adb -s ${serial} reboot recovery`);
        await run(`adb -s ${serial} wait-for-recovery`);
        await run(`adb -s ${serial} shell twrp install /sdcard/Rom/super_gapps.zip`);
        await run(`adb -s ${serial} shell sleep 5`);
        await run(`adb -s ${serial} reboot`);
        console.log(`done for ${serial}`);
        res(true);
      })
  );
  await Promise.all(runingDevices);
  console.log(`-------------------------done for all----------------------------------`);
};
const setupRoom = async (devices) => {
  if (!devices.length) {
    devices = await getDevices();
  }
  return setup(devices.filter((e) => e.serial && e.serial.length == 18));
};

setupRoom([]);
