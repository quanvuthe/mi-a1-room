const util = require('util');
const exec = util.promisify(require('child_process').exec);
const sleep = (timeout) => new Promise((res) => setTimeout((res, timeout)));

const run = async (cmd) => {
  try {
    console.log('run =>', cmd);
    const { stdout, stderr } = await exec(cmd, {
      maxBuffer: 1024 * 1024 * 1024,
    });
    return { stdout, stderr };
  } catch (err) {
    return { stdout: '', stderr: err.message };
  }
};
const getDevices = async () => {
  try {
    const adbDevices =
      (await run('adb devices'))?.stdout
        ?.match(/(\w+)\t/g)
        ?.map((x) => ({ serial: x.replace('\t', ''), type: 'adb' }))
        ?.sort() || [];
    const fastbootDevices =
      (await run('fastboot devices'))?.stdout
        ?.match(/(\w+)\t/g)
        ?.map((x) => ({ serial: x.replace('\t', ''), type: 'fastboot' }))
        ?.sort() || [];
    return [...adbDevices, ...fastbootDevices];
  } catch (err) {
    console.log({ err });
    return [];
  }
};

const setupA1 = async (devices) => {
  const runingDevices = devices.map(
    (device) =>
      new Promise(async (res) => {
        const { serial, type } = device;
        if (type === 'adb') {
          await run(`adb -s ${serial} reboot bootloader`);
          await run(`fastboot -s ${serial} wait-for-device`);
        }
        const bootRoom = await run(`fastboot -s ${serial} boot A1/Rom/recovery.img`);
        if (bootRoom.stderr?.includes('unlock device to use')) {
          await run(`fastboot -s ${serial} flashing unlock`);
          console.log('done for', serial);
          return;
        } else {
          await run(`adb -s ${serial} wait-for-recovery`);
          await sleep(1000);
          await run(`adb -s ${serial} shell "twrp wipe data"`);
          await sleep(1000);
          await run(`adb -s ${serial} shell "twrp wipe cache"`);
          await sleep(1000);
          await run(`adb -s ${serial} shell "twrp wipe dalvik"`);
          await sleep(1000);
          await run(`adb -s ${serial} shell "twrp wipe system"`);
          await sleep(1000);
          await run(`adb -s ${serial} push A1/Rom /sdcard/`);
          await run(`adb -s ${serial} shell twrp install /sdcard/Rom/xpro_rom.zip`);
          await run(`adb -s ${serial} shell twrp install /sdcard/Rom/xpro_recovery.zip`);
          await run(`adb -s ${serial} reboot recovery`);
          await run(`adb -s ${serial} wait-for-recovery`);
          await run(`adb -s ${serial} shell twrp install /sdcard/Rom/super_gapps.zip`);
          await run(`adb -s ${serial} reboot recovery`);
          await run(`adb -s ${serial} wait-for-recovery`);
          await run(`adb -s ${serial} shell twrp mount system`);
          await run(`adb -s ${serial} shell rm -rf /system/system/priv-app/twrpapp`);
          await run(`adb -s ${serial} shell rm -rf /data/data/me.twrp.twrpapp`);
          await run(`adb -s ${serial} reboot`);
        }
        console.log(`done for ${serial}`);
        res(true);
      })
  );
  await Promise.all(runingDevices);
  console.log(`-------------------------done for all----------------------------------`);
};

const setupRoom = async (devices) => {
  if (!devices.length) {
    devices = await getDevices();
  }
  return setupA1(devices.filter((e) => e.serial && e.serial.length == 12));
};

setupRoom([]);
