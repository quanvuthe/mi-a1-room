const util = require('util');
const exec = util.promisify(require('child_process').exec);
const sleep = (timeout) => new Promise((res) => setTimeout(res, timeout));

const run = async (cmd) => {
  try {
    console.log('run =>', cmd);
    const { stdout, stderr } = await exec(cmd, {
      maxBuffer: 1024 * 1024 * 1024,
    });
    return { stdout, stderr };
  } catch (err) {
    return { stdout: '', stderr: err.message };
  }
};
const getDevices = async () => {
  try {
    const adbDevices =
      (await run('adb devices'))?.stdout
        ?.match(/(\w+)\t/g)
        ?.map((x) => ({ serial: x.replace('\t', ''), type: 'adb' }))
        ?.sort() || [];
    const fastbootDevices =
      (await run('fastboot devices'))?.stdout
        ?.match(/(\w+)\t/g)
        ?.map((x) => ({ serial: x.replace('\t', ''), type: 'fastboot' }))
        ?.sort() || [];
    return [...adbDevices, ...fastbootDevices];
  } catch (err) {
    console.log({ err });
    return [];
  }
};
const setupS9 = async (devices) => {
  const runingDevices = devices.map(
    (device, index) =>
      new Promise(async (res) => {
        const { serial } = device;
        // await run(`adb -s ${serial} shell "timeout -s 7 twrp format data"`);
        await sleep(index * 10000);
        console.log(`START ${index}/${devices.length}: ${serial}`);
        await run(`adb -s ${serial} reboot recovery`);
        await run(`adb -s ${serial} wait-for-recovery`);
        await run(`adb -s ${serial} shell rm -rf /sdcard/*`);
        await run(`adb -s ${serial} push S9_20/Rom /sdcard/`);
        await run(`adb -s ${serial} push S9_20/TWRP /sdcard/`);
        await run(`adb -s ${serial} shell sleep 5`);
        await run(`adb -s ${serial} shell "timeout -s 5 twrp wipe data"`);
        await run(`adb -s ${serial} shell "timeout -s 5 twrp wipe cache"`);
        await run(`adb -s ${serial} shell "timeout -s 5 twrp wipe dalvik"`);
        await run(`adb -s ${serial} shell "timeout -s 5 twrp wipe system"`);
        await run(`adb -s ${serial} shell sleep 5`);
        await run(`adb -s ${serial} shell twrp install /sdcard/Rom/super_rom_S9.zip`);
        await run(`adb -s ${serial} shell sleep 5`);
        await run(`adb -s ${serial} shell twrp install /sdcard/Rom/super_gapps_S9.zip`);
        await run(`adb -s ${serial} shell sleep 5`);
        await run(`adb -s ${serial} shell twrp install /sdcard/Rom/no-verity.zip`);
        await run(`adb -s ${serial} shell sleep 5`);
        await run(`adb -s ${serial} shell twrp restore /sdcard/TWRP/BACKUPS/data`);
        await run(`adb -s ${serial} shell sleep 5`);
        await run(`adb -s ${serial} reboot`);
        console.log(`DONE ${index}/${devices.length}: ${serial}`);
        res(true);
      })
  );
  await Promise.all(runingDevices);
  console.log(`-------------------------done for all----------------------------------`);
};
const setupRoom = async (devices) => {
  if (!devices.length) {
    devices = await getDevices();
  }
  return setupS9(devices.filter((e) => e.serial && e.serial.length === 16));
};

setupRoom([]);
